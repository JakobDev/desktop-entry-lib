import desktop_entry_lib


def main() -> None:
    collection = desktop_entry_lib.DesktopEntryCollection()

    # Load all desktop entries from the Menu into the Collection
    collection.load_menu()

    print("This Apps can open HTML files:")
    for i in collection.get_entries_by_mime_type("text/html"):
        print(i.Name.get_translated_text())

    # Check if desktop entry is in the Collection
    if "org.example.app" not in collection:
        print("org.example.app was not found")
        return

    # Do something with the entry
    print(collection["org.example.app"].Name.get_translated_text())


if __name__ == "__main__":
    main()
