import desktop_entry_lib


def main() -> None:
    entry = desktop_entry_lib.DesktopEntry.from_file("my_app.desktop")

    print("Name: " + entry.Name.default_text)
    print("Comment: " + entry.Comment.default_text)
    print("German translation for Comment: " + entry.Comment.translations.get("de", "None"))
    print("Type: " + entry.Type)
    print("Exec: " + entry.Exec)


if __name__ == "__main__":
    main()
