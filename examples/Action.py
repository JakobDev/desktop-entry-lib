import desktop_entry_lib


def write() -> None:
    entry = desktop_entry_lib.DesktopEntry()

    entry.Name.default_text = "My App"

    action = desktop_entry_lib.DesktopAction()
    action.Name.default_text = "My Action"

    entry.Actions["Example"] = action

    entry.write_file("my_app.desktop")


def read() -> None:
    entry = desktop_entry_lib.DesktopEntry.from_file("my_app.desktop")

    print(entry.Actions["Example"].Name.default_text)


if __name__ == "__main__":
    write()
    read()
