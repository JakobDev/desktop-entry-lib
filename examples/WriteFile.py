import desktop_entry_lib


def main() -> None:
    entry = desktop_entry_lib.DesktopEntry()

    entry.Name.default_text = "My App"
    entry.Comment.default_text = "A short description"
    entry.Comment.translations["de"] = "A short german description"
    entry.Type = "Application"
    entry.Exec = "my-app"

    entry.write_file("my_app.desktop")


if __name__ == "__main__":
    main()
