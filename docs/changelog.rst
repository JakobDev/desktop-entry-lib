Changelog
==========================

-------------------------
2.3
-------------------------
- Add StartupWMClass key
- Fix should_show_in_menu()
- Allow : as suffix for path environment variables

-------------------------
2.2
-------------------------
- Save boolean values as lowercase
- Fix DBusActivatable key

-------------------------
2.1
-------------------------
- Fix saving Terminal Key

-------------------------
2.0
-------------------------
- Add support for Actions
- Add support for Flatpak
- Implement magic methods
- Implement validation

-------------------------
1.0
-------------------------
- First release