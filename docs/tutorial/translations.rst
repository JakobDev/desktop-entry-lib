Translations
==========================
As you should now, some keys of desktop entries can be translated. desktop entry-lib provides the TranslatableKey class to support this.

TranslatableKey class has 2 attributes:
default_text: This is a string which contains the non translated text.
translations: This is a dict, which has the language as key and the text as value

Take for example this part of a desktop entries:

.. code:: ini

    Name=MyApp
    Name[de]=MyAppGerman
    Name[fr]=MyAppFrench

If you load this, with desktop-entry-lib, the default_text attribute would be MyApp and the translations dict would have the 2 keys de and fr with the corresponding value.