Desktop Actions
==========================
Desktop entries can contain additional actions. Take for example this:


.. code:: ini

    [Desktop Entry]
    Type=Application
    Name=Test
    Actions=MyAction;

    [Desktop Action MyAction]
    Name=Example Action

Each instance of DesktopEntry has a Actions attribute to support this. It is a dict. The Key is the internal name (in this case MyAction).The value is a instance of DesktopAction, which can basically be used the same way as DesktopEntry.