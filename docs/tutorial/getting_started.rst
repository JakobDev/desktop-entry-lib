Getting Started
==========================
desktop-entry-lib allows reading and writing .desktop files.

The class for handling desktop entries is the  DesktopEntry class. You can create a new empty DesktopEntry be creating a new instance of the class, or you can load a existing one with these 3 classmethods:
 - from_string(text): This functions takes the content of a .desktop file as argument.
 - from_file(path): This function takes the path to a .desktop file as argument.
 - from_id(desktop_id): This function searches all the places of your system, where desktop usually life. If it found one with the id (e.g. the ID is org.example.app, it will search for org.example.app.desktop) it will load it.

The class Attributes of the class are the Keys of the desktop entry. e.g. If you want to access the value of the Key Type, you use my_object.Type. If a desktop entry does not have this Key, what can happens if a Key is optional, the Value will be None.