Collection
==========================
desktop-entry-lib provides the DesktopEntryCollection class. As the name suggests, it is a collection of desktop entries.

You begin by creating a new instance of DesktopEntryCollection. There are 4 different ways to add DesktpEntries to the Collection.
 - load_file(path): This adds a single file to the Collection. It takes the path to the .desktop file as argument.
 - load_directory(path): This adds all .desktop files in this directory to the Collection. It takes the path to the directory as argument.
 - load_menu(): This adds all .desktop files that you see in your Menu to the Collection.
 - load_desktop(): This adds all .desktop files, that you have on your Desktop to the Collection.

You can call this functions as often as you want and add as many DesktopEntries as you like.

DesktopEntryCollection has a attribute called desktop_entries. It is a dict. The Key is the ID of the file. It is determined by the file name. If a file is called org.example.app.desktop the ID will be org.example.app.
If you load another file with the same name, the old one it will be overwritten in the Collection.