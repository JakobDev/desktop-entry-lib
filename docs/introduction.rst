Introduction
==========================
desktop-entry-lib allows reading and writing .desktop files according to the `Desktop Entry Specification <https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html>`_.

-------------------------
Installation
-------------------------
You can install desktop-entry-lib like any other package with pip:

.. code::

    pip install -U desktop-entry-lib

Since desktop-entry-lib is only `a single file <https://gitlab.com/JakobDev/desktop-entry-lib/-/blob/main/desktop_entry_lib/__init__.py>`_, you can also just copy it into your project.

-------------------------
Supported OS
-------------------------
desktop-entry-lib is primarily made for Linux and other OS, that support the Desktop Entry Specification, but it also works on other OS like Windows.
It will miss a few fOS dependent features like reading all files from your Desktop, but the rest is working, so you can use it the create .desktop files
for Linux for your project, even if you use a other OS.

-------------------------
Validation
-------------------------
If you want to use the validation, you need desktop-file-validate to be installed and in PATH.

-------------------------
Flatpak
-------------------------
desktop-entry-lib supports running inside a `Flatpak <https://flatpak.org/>`_. and uses the right Paths.