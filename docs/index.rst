.. desktop-entry-lib documentation master file, created by
   sphinx-quickstart on Fri Jul 22 11:00:44 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to desktop-entry-lib's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   tutorial/index
   api
   examples/index
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
